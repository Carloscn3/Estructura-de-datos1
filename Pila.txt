using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodo
{
    public class Nodo
    {
        private int info;
        public Nodo sig;

        public Nodo(int info)
        {
            this.info = info;
        }

        public int Ver()
        {
            return info;
        }
    }
    public class Pila
    {
        private Nodo tope;
        public void Push(Nodo nuevo)
        {
            if (tope == null)
                tope = nuevo;
            else
            {
                nuevo.sig = tope;
                tope = nuevo;
            }
        }
        public Nodo Pop()
        {
            Nodo tempo;
            if (tope != null)
            {
                tempo = tope;
                tope = tope.sig;
                return tempo;
            }
            return null;
        }
    }
    class MainClass
    {
        public static void Main(string[] args)
        {
            Pila p = new Pila();
            Nodo Mostrar;
            for (int ciclo = 0; ciclo < 5; ciclo++)
                p.Push(new Nodo(ciclo + 1));
            do
            {
                Mostrar = p.Pop();
                if (Mostrar != null)
                    Console.WriteLine(Mostrar.Ver());
            } while (Mostrar != null);
            Console.ReadKey();
        }
    }
}
